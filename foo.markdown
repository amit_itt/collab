![](media/image1.png){width="1.675in" height="0.4673611111111111in"}

SRS Card Analyzer

*Revision 1.3.5*

<span id="_Toc393957166" class="anchor"></span>Revision History

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **Version**   **Name**        **Reason For Changes**                                                                                                                                                                                                                                                                                                                                                                 **Date**
  ------------- --------------- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ------------
  *1.0.0*       Carl Walthall   Draft version                                                                                                                                                                                                                                                                                                                                                                          11/18/14

  1.0.1         Carl Walthall   Added information on the search Algorithm and reader new table.                                                                                                                                                                                                                                                                                                                        12/8/2014

  1.0.2         Carl Walthall   Changed 125 KHz search logic                                                                                                                                                                                                                                                                                                                                                           12/9/2014

  1.0.3         Carl Walthall   Added new screen shots, logic details and additional text                                                                                                                                                                                                                                                                                                                              12/11/2014

  1.0.4         Carl Walthall   Added new screen shots for push buttons on the auto learn tab and additional dialog                                                                                                                                                                                                                                                                                                    12/12/2014

  1.0.5         Carl Walthall   Added phase 3 of the project                                                                                                                                                                                                                                                                                                                                                           12/14/2014

  1.0.6         Carl Walthall   Changed tabs to windows for the card analyzer                                                                                                                                                                                                                                                                                                                                          12/17/2014

  1.0.7         Carl Walthall   Changed phase 1 description                                                                                                                                                                                                                                                                                                                                                            1/7/2015

  1.0.8         Carl Walthall   Added new application screen shots                                                                                                                                                                                                                                                                                                                                                     3/17/2015

  1.0.9         Carl Walthall   Added screen information for Analyzes process                                                                                                                                                                                                                                                                                                                                          3/18/2015

  1.1.0         Carl Walthall   Added a process flow diagram to this document                                                                                                                                                                                                                                                                                                                                          3/19/2015

  1.1.1         Carl Walthall   Changed buttons on screen shots and logic for them.                                                                                                                                                                                                                                                                                                                                    3/23/2015

  1.1.2         Carl Walthall   Changed name of the write reader screen. Fixed logic on analyze card screen.                                                                                                                                                                                                                                                                                                           3/25/2015

  1.1.3         Carl Walthall   Added the use of the bit and byte twister to analyze                                                                                                                                                                                                                                                                                                                                   3/26/2015

  1.1.4         Akash Mittal    Update the Auto Config screen shots and added data format mode validation in reader detection                                                                                                                                                                                                                                                                                          4/17/2015

  1.1.5         Carl Walthall   Added information on selecting the card type after completing the Learn Card search and updated the process diagram.                                                                                                                                                                                                                                                                   4/17/2015

  1.1.6         Carl Walthall   Removed radial buttons, and reorganized text fields and buttons.                                                                                                                                                                                                                                                                                                                       4/30/2015

  1.1.7         Carl Walthall   Corrected information on iClass and type of reader connected. Added information in the Analyze Card tab on configuration on the bit twister and 64 bit math enable.                                                                                                                                                                                                                    5/4/2015

  1.1.8         Carl Walthall   Corrected some of the grammar and logic to make clearer. Corrected ID Extended precision math usages and Parity table.                                                                                                                                                                                                                                                                 5/5/2015

  1.1.9         Carl Walthall   Changed movement buttons, configuration write select, Card ID Dialog and removed temporary buffer. Also changed reader configuration to Beeper off.                                                                                                                                                                                                                                    5/18/2015

  1.2.0         Carl Walthall   Added text over the screen shots and cleanup some of the text.                                                                                                                                                                                                                                                                                                                         5/28/2015

  1.2.1         Carl Walthall   Resized screen fields and changed supporting readers for Indala and CASI cards. Also change rules for detecting ID Teck and Indala card Types.                                                                                                                                                                                                                                         5/29/2015

  1.2.2         Carl Walthall   Correct EM card supporting reader model number. Added new search algorithm.                                                                                                                                                                                                                                                                                                            6/8/2015

  1.2.3         Carl Walthall   Replaced Indala UID Card Types and changed notes. Removed the status window on the Auto Config screen. Added the halt scan button on the learn screen. Resized the field boxes and made more room around the buttons all screens.                                                                                                                                                      6/18/2015

  1.2.4         Carl Walthall   Updated the Auto Analyze search                                                                                                                                                                                                                                                                                                                                                        8/8/2015

  1.2.5         Akash Mittal    Update the screen shots and added Anlayze Flow with the document                                                                                                                                                                                                                                                                                                                       9/11/2015

  1.2.6         Carl Walthall   New search for MIFARE and HID cards. Added support for RDR-80082AKU and RDR-80582AKU readers. Added raw data to Learn screen. Changed the Learn Card button on the Analyzer screen. Added check box to create a HWG file during write reader. Added reader link to website on the Learn Card screen.                                                                                   10/26/2014

  1.2.7         Dheeraj Jha     Reader in “Extended” mode, the application shall place the reader in Standard mode, Updated the Reader table for (Nano readers, Reader naming convention), Filter rule of Mifare and HID, Need to display original card type on Auto Config instead of OFF, Need to maintain equal space among the buttons of all wizard pages, Save Hwg + button on Analyze and Auto Config screen.   11/05/2015

  1.2.8         Carl Walthall   Added support for the Loon reader                                                                                                                                                                                                                                                                                                                                                      6/16/2016

  1.2.9         Carl Walthall   Adding new rules to the scan card list for ISO 14443A and ISO14443B card types                                                                                                                                                                                                                                                                                                         7/17/2016

  1.3.0         Amit Jain       Added Amit’s design for the 14443A and ISO14443B card types                                                                                                                                                                                                                                                                                                                            7/18/2016
                                                                                                                                                                                                                                                                                                                                                                                                                       
                Carl Walthall                                                                                                                                                                                                                                                                                                                                                                                          

  1.3.1         Carl Walthall   Corrected iCLASS logic for readers                                                                                                                                                                                                                                                                                                                                                     7/18/2016

  1.3.2         Saloni Ojha     Change the card names according to new cardtype.txt                                                                                                                                                                                                                                                                                                                                    8/8/2016

  1.3.3         Amit Jain       Updated reader note and reader URL                                                                                                                                                                                                                                                                                                                                                     8/08/2016

  1.3.4         Carl Walthall   Changed texted for supporting readers and added new logic for AWID and ISONAS Card Types                                                                                                                                                                                                                                                                                               8/11/2016

  1.3.5         Amit Jain       Updated reader notes and card names                                                                                                                                                                                                                                                                                                                                                    8/23/2016
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

<span id="_Toc393957167" class="anchor"><span id="_Toc59341540"
class="anchor"><span id="_Toc55009008"
class="anchor"></span></span></span>

Approved By

  **Name**   **Signature**   **Department**   **Date**
  ---------- --------------- ---------------- ----------
                                              
                                              
                                              
                                              
                                              

<span id="_Toc393957168" class="anchor"><span id="_Toc439994665"
class="anchor"></span></span>

**\
Introduction**

<span id="_Toc393957169" class="anchor"><span id="_Toc459197038" class="anchor"></span></span>Purpose
=====================================================================================================

> This document contains the requirements for phase 1, 2 and 3 of the
> RFIDeas Card Analyzer project. It covers the requirements,
> architecture, and logic for development during each phase. The
> developer must follow modular development practices as each of the
> building blocks may operate separately or in a group.

<span id="_Toc393957170" class="anchor"><span id="_Toc459197039" class="anchor"></span></span>Definitions and Acronyms
======================================================================================================================

> **SYRS** – System Requirement Specifications
>
> **ID** – Identification
>
> **User** – The person using the application.
>
> **Hex mode** –Output of card data will display in Hexadecimal format.
>
> **Decimal mode** – Output of card data will display in Decimal format.
>
> **Standard Reader** – Legacy or Plus readers without bit field
> configuration support.

Test Equipment
==============

  **Equipment**   **Name**
  --------------- ------------------------------------------------
  RDR-80581AKU    pcPlus Plus Reader
  RDR-80081AKU    pcProx Plus Reader
  RDR-80582AKU    pcPlus Plus Reader
  RDR-80082AKU    pcProx Plus Reader
  Card Types \`   Card Types located in table 2 of this document
  RDR-805x1AxU    Loon AKU reader KS
  RDR-805x2AxU    Loon AKU reader SDK
  RDR-800x1AxU    Loon AKU reader KS
  RDR-800x2AxU    Loon AKU reader SDK

Architecture:
=============

> The architecture consists of three modules, Auto Learn, Auto
> Configuration, and Auto Analyze (Figures 1 and 1A) below. The Auto
> Learn provides card information and raw data to the Configuration and
> Analyze processes. The Learn, Configuration and Analyze modules have
> user interfaces for providing Card Type, Card ID and supporting reader
> information to the user.

Auto Analyze Processes:
-----------------------

Figure 1

Process Flow:
-------------

![](media/image3.emf){width="6.466663385826772in"
height="1.3020833333333333in"}

Figure 1A

### Welcome Screen:

> The “Welcome” screen is the entry point to the Card Analyzer
> application. It describes the usage of this application to the user
> and determines if the correct reader is connected.

###  Learn Card:

> This process is responsible for searching for the presented Card Type
> and contains the search algorithm. After searching the card list the
> following features are enabled. If the application was unable to
> locate the presented card, the user has the options to scan “Scan” for
> a new Card Type or press the application “Exit” button.
>
> If a card was found; the application will make the “Auto Config”
> button available. This will enable the user to transition to the Auto
> Configuration screen if the reader is a RDR-80081AKU, RDR-800x1AxU,
> RDR-80581AKU or the RDR-805x1AxU. The Auto Configuration and Analyze
> screens are unavailable for the RDR-80082AKU, RDR-800x2AxU,
> RDR-80582AKU or the 805x2AxU SDK readers.

### Auto Configuration:

> In this mode, the analyzer will display the Card Types found, and the
> ID for the Card Type selected. The user has the options to press the
> “&lt; Learn Card” button to scan for a new card, or select the
> configuration number and press the “Write” button to write the default
> card settings to the reader.

### Analyze:

> If the user has the ID number, the Analyzer process will attempt to
> determine the LP, TP, and ID Bits field settings. The raw data and
> available Card Types are passed from the Auto Configuration process to
> the Analyzer process. If the user presses the “Write” button prior to
> analyzing the FAC or ID values; the application shall write the
> default card settings to the reader.

###  Write Reader or Exit Application:

> The user can exit the Card Analyzer application at any time. If none
> of the configurations for the legacy Plus or Loon readers are written.
> The application shall restore the original reader setting saved during
> startup.

Card Analyzer Selection
=======================

<span id="_Toc406493829" class="anchor"><span id="_Toc459197050" class="anchor"></span></span>Card Analyzer Dropdown Menu:
--------------------------------------------------------------------------------------------------------------------------

> The application shall contain a drop down menu to select the Card
> Analyzer feature of the pcProxConfig application. When selected the
> application shall start a new thread blocking access to the
> pcProxConfig GUI while the analyzer feature is active.
>
> ![](media/image4.png){width="6.5in" height="1.3333333333333333in"}

Figure 2

Auto Learn Process:
-------------------

> The Auto Learn process contains the logic for scanning cards to
> determine the Card Type, gather raw data, and present a list of
> readers that support the Card Type.
>
> The details of the Card Types are located in the “Card Type and
> available readers” selection of this document. The table contains the
> Card Types a list of reader model numbers who support the Card Type
> and variants.
>
> The application shall use the “Card Type” table to poll the reader.
> The reader shall respond with the raw data for the detected Card Types
> when a match is found. To make sure the Card Types for the application
> and reader stay in sync. The application must compare the list of Card
> Types with the readers list. The results of the compare will determine
> the search list.

Welcome Screen:
===============

> The Welcome Screen (Figure 3) provides a brief description of the
> analyzers capability.

Welcome Screen Text:
--------------------

> “The Card Analyzer will search for all card types available to the
> pcProx Plus reader. The application will use this information to
> display our supporting readers and Card Types. It also gives the user
> the option to select and write our default settings to the reader. If
> the user doesn’t know the card information, the utility will determine
> the correct settings and give the user the option to write them to the
> reader.”
>
> The RFIDeas marketing group will determine the actual text for this
> screen.

Reader Detection:
-----------------

> During startup, the “Card Analyzer” application, shall determine if
> the attached reader is one of the supported readers listed in the
> Learn Card section of this document. The application must save the
> current configuration to a HWG+ file; set all the configurations to
> off; place the reader in Standard mode and “Write Active” the
> settings. After completing the “Write Active” command; the application
> shall reboot the reader. These events shall unlock the reader if a
> card was placed on the reader prior to starting the Auto Learn
> process. If the readers in “Extended”, the application shall place the
> reader in Standard mode after saving the current configurations to a
> HWG file.
>
> The reader’s connection status shall be displayed at the bottom of the
> “Welcome” screen (Figure 3).

### Reader Status Messages:

> The status line shall provide feedback to the user on the state of the
> connected reader (Figure 3).
>
> **Example:**

1.  **“Status: Reader not connected”**

2.  **“Status: Incorrect reader connected, please use a RDR-80081AKU,
    RDR-80082AKU, RDR-80581AKU, RDR-80582AKU, RDR-800x1AxU,
    RDR-800x2AxU, RDR-805x1AxU or RDR-805x2AxU” reader.**

3.  **“Status: Reader connected”**

### Welcome Screen Push Buttons:

![](media/image5.png){width="5.651475284339457in"
height="4.677083333333333in"}

> Figure 3A
>
> The push buttons on the “Welcome” screen (Figure 3A) shall follow
> these rules.

-   Disable the “Back” button on this screen (No text on button).

-   The “Learn Card&gt;” button is disabled if the reader was
    disconnected or the wrong model is detected.

-   The “Learn Card&gt;” button shall become active when connected to
    the correct reader. Pressing this button shall cause the application
    to transition to the “Learn Card” screen.

-   The “Exit” button is always active. When pressed, the “Card
    Analyzer” application shall close making the pcProxConfig GUI
    active again.

Reader Configuration Raw and Formatted:
=======================================

> The application shall configure the reader to send raw data. Set the
> LP, TP to zero, Invert Bits to on, 64 bit math on, the beeper off and
> place the reader in SDK mode. Because we are using both configurations
> to scan for the presented card, both priority are enabled,
> configuration 1 and 2 are set to on. The application shall poll the
> reader to capture the raw card data.
>
> The formatted data is derived using the default LP, TP, Bit control,
> and Byte settings found in the default Card Type table during the
> “Auto Configuration” process.

Scan Frequencies:
=================

> The application shall scan for MIFARE, HID then the remaining
> Contactless 13.56 MHz, and Proximity 125 KHz Card Types each time the
> user presses the “Start Scan” button (Figure 5) below. After
> completing the learn card scan; the application shall turn all
> configurations off; write the settings active and reboot the reader.

The Learn Card Screens:
=======================

> ![](media/image6.png){width="5.915927384076991in"
> height="4.947298775153106in"}
>
> Figure 4
>
> The Learn Card function starts when the user presses the “Learn Card”
> button on the “Welcome” screen. The “Back” button is grayed out on
> this screen.
>
> ![](media/image7.png){width="5.572916666666667in"
> height="4.640894575678041in"}
>
> Figure 5

Learn Card screen text:
-----------------------

> The following text shall be displayed over the reader picture (Figure
> 4).
>
> “In this step we will attempt to learn the card presented to the
> reader. The results of the scan will provide a list of readers
> supporting the presented card.
>
> 1: Press the “Start Scan” button to learn the card.
>
> 2: Follow the card placement instructions displayed in the popup and
> status boxes.
>
> 3: Pressing the “Halt Scan” button will stop the card search.
>
> 4: Press the “Exit” button to stop the Card Analyzer and return to the
> configuration utility.”

Card Placement Popup:
---------------------

> Clicking the “Start Scan” button shall popup a dialog box instructing
> the user to place the card on the Reader (Figure 5). After pressing
> the ok button, the dialog box shall close and the “Start Scan” button
> is disabled; the “Halt Scan” button is enabled.

Learn Screen buttons:
---------------------

> The “Back” and “Auto Config” buttons are grayed out and disabled while
> scanning. The “Auto Config” button shall become active after a card is
> detected and the scanning has completed, (Figure 5A) below.
>
> ![](media/image8.png){width="5.645127952755906in"
> height="4.697329396325459in"}
>
> Figure 5A
>
> The user can halt the card search process by pressing the “Halt Scan”
> button (Figure 5A). The application shall complete the current card
> scan, set the scan to the beginning of the MIFARE technology list,
> clear the dialog box, display “Press the Start Scan button to learn a
> new card” in the status frame and waits for the user to press the
> “Start Scan” button (Figure 4).
>
> Pressing the “Start Scan” button (Figures 4) and pressing the “Ok”
> button on the popup, restarts the “Card Type” search.

 **Scan Complete:** 
--------------------

> ![](media/image9.png){width="5.707620297462817in"
> height="4.718160542432196in"}
>
> Figure 6
>
> If the application doesn’t detect a card; the status message; “Card
> not found: Please contact RFIDeas for additional support” (Figure 6)
> shall be displayed. The user can start another card scan by placing
> the new card on the reader and pressing the “Start Scan” button.
> Pressing the exit button will leave the card analyzer application.
>
> ![](media/image10.png){width="5.728451443569554in"
> height="4.749406167979003in"}
>
> Figure 6A
>
> After completing the “Learn Card” search (Figure 6A), the application
> shall high light the first Card Type displayed is the “Card Type” box.
> The “Supporting Readers” dialog box shall display the list of readers
> supporting this Card Type. The user has the ability to select any of
> the Card Types found in the list. The supporting reader dialog box
> shall update accordingly. This rule will keep the application from
> displaying duplicate reader model numbers. These rules apply to all
> the dialog boxes displaying the “Card type” and “Supporting Readers”
> information.
>
> After completing the “Learn Card” process, the application shall
> enable the “Auto Config” button if the card is detected. The user can
> press the “Auto Config” button to transition to the “Auto Config”
> screen (Figure 6A), or “Learn Card” to learn a new Card Type or “Exit”
> to leave the Card Analyzer application. The following information
> shall be displayed to the user for each of the detected Card Types.

-   The application shall display formatted data in the “Card ID” field
    for the Card Type highlighted.

-   The blue bar represents the percentage of time needed to complete
    the card scan shall be removed.

> The Learn Card screen shall display the number of bits and raw data of
> the selected card type as represented in Figure 6A. Double clicking on
> the bit status line will perform the same action as this line does on
> the main utility, a popup box will appear telling the user the number
> of bit and raw data was copied to the buffer.
>
> The user can double click any of the readers displayed in the
> supporting reader list box. The application shall open a web browser,
> displaying information about the selected reader.
>
> Reader Links:

  Reader                                                 Link
  ------------------------------------------------------ --------------------------------------------------------------------------
  RDR-601x,                                              https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-601
  RDR-602x                                               https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-602
  RDR-608x,                                              https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-608
  RDR-628x                                               https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-628
  RDR-631x                                               https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-631
  RDR-632x                                               https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-632
  RDR-638x                                               https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-638
  RDR-647x,                                              https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-647
  RDR-678x                                               https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-678
  RDR-691x                                               https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-691
  RDR-692x                                               https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-692
  RDR-698x                                               https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-698
  RDR-6A8x,                                              https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-6A8
  RDR-6C8x,                                              https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-6C8
  RDR-6E1x                                               https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-6E1
  RDR-6E2x                                               https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-6E2
  RDR-6E8x                                               https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-6E8
  RDR-6G8x,                                              https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-6G8
  RDR-6H8x,                                              https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-6H8
  RDR-6K8x                                               https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-6K8
  RDR-6N8x,                                              https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-6N8
  RDR-6R8x,                                              https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-6R8
  RDR-6Z8x,                                              https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-6Z8
  RDR-708x                                               https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-708
  RDR-75xx                                               https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-758
  RDR-805xx                                              https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-805
  RDR-800xx                                              https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-800
  RDR-708x for iCLASS ID/CSN                             https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-708
  RDR-758x for iCLASS CSN                                https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-758
  RDR-805xx for iCLASS CSN                               https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-805
  RDR-800xx for iCLASS ID (SEOS requires FW LNCxxxxxx)   https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-800
  RDR-805xx (requires FW LNCxxxxxx)                      https://www.rfideas.com/products/search?field\_clean\_sku\_value=RDR-805
                                                         

Auto Configuration Screen:
==========================

![](media/image11.png){width="5.572220034995626in"
height="4.645252624671916in"}

> Figure 7
>
> When the user transitions to the “Auto Config” screen (Figure 7), the
> analyzer shall display the number of bits and raw data. Double
> clicking on the bit status line will perform the same action as it
> does in the main utility. A popup box will appear and display the
> number of bits and raw data copied to the scratch buffer.
>
> **NOTE:** When the user transitions to the Auto Configuration screen,
> Configurations 1 and 2 will display the last card types written to the
> reader.
>
> ![](media/image12.png){width="5.624296806649169in"
> height="4.728576115485565in"}
>
> Figure 7A

Text over picture, Auto Configuration screen:
---------------------------------------------

> The following text shall be displayed over the reader picture (Figure
> 7A).
>
> “In this step we will display the Card ID found using our default
> reader setting. Highlight each of the cards listed in the Card Type
> field and verify the ID number.
>
> 1: If the number matches your ID, follow these steps.
>
> 2: Select the Configuration \# you wish to write and press the write
> button.
>
> 3: If you’re unable to find your ID in the Card Type list. Press the
> “Learn” button and try a new card or press the “Analyze” button and we
> will attempt to find the card settings for you.
>
> 4: Press the “Exit” button to stop the Card Analyzer and return to the
> configuration utility.”

Card Type Field:
----------------

> The field is a list box (Figures 7A) that allows the user to select
> one of the available Card Types found during the “Learn Card” process.
> When the user selects a “Card Type”, the application shall display the
> formatted Card ID for the selected Card Type in the “Card ID” text
> field.

Card ID:
--------

> The “Card ID” field shall display the formatted data converted using
> the default card settings for the Card Type selected in the “Card
> Type” field (Figure 7A). The default card settings are located in the
> Card Type list file.

Auto Configuration Write Button:
--------------------------------

> This field displays the current Card Types written to the Reader
> before entering the Card Analyzer application. The “Write” button is
> used in conjunction with the “Configuration \#” selection display
> field. Pressing the “Write” button will write the new Card Type
> selected in the Card Type field to the reader. Changing the
> configuration number will display the Card Type in the reader’s
> remaining configurations. The numbers of available configurations are
> determined during connection to the reader (2 or 4).

<span id="_Toc434490592" class="anchor"><span id="_Toc459197069" class="anchor"></span></span>Auto Configuration Save Hwg + Button:
-----------------------------------------------------------------------------------------------------------------------------------

> The Save HWG file button will always save the temporary file to a HWG
> file with current reader settings when pressed, to the specified
> location.

Auto Configuration Screen Analyze button:
-----------------------------------------

> The “Analyze” button is always available on this screen. Pressing it
> will cause the application to transition to the “Analyze Card” screen.

Auto Configuration Screen Exit Button: 
---------------------------------------

1.  If the user exits the application without writing to the reader, the
    application shall reload the saved HWG+ file.

2.  If one of the configurations are written to the reader, the others
    shall remain in their current settings. The application shall reload
    the saved HWG+ file if none of the configurations are changed.

3.  If one or all the configurations are written to the reader, the
    application shall not reload the saved HWG+ file.

> If the user presses the “Write” or “Exit” buttons, the Card Analyzer
> function shall create a popup dialog box instructing the user to
> “Remove the card from the reader, and press the OK button to
> continue”.

![](media/image13.png){width="3.3641633858267714in"
height="1.8018580489938758in"}

**Card Auto Learn Contactless** 13.56 MHz and Proximity 125 KHz**:**
====================================================================

> The search algorithm will search the list of Card Types starting with
> the HID and MIFRE; then the remaining Contactless and Proximity cards.
> The design shall make it easy to adjust the search order. The user can
> halt the search at any time by pressing the “Halt Scan” button (Figure
> 5A) above.

Flow 1

Flow 2

Card Type and Reader Selection:
-------------------------------

> Table 1 contains the list of Card Types and supporting readers.
>
> **Example: **
>
> If the Auto Learn function determines the Card Type is HID Prox, the
> reader to purchase is a RDR-608x, RDR-805xx, RDR-800xx (Note 7).

  -------------------------------------------------------------------------------------
  Card Type   Card Manufacture                                    Reader    Freq
  ----------- --------------------------------------------------- --------- -----------
  0100        HID iCLASS ID (iClass SE)                           Note 1    13.56 MHz

  0101        HID SEOS                                            Note 24   13.56 MHz

  6F01        RDR-758x Equivalent(iClass/ISO14443A/ISO15693)CSN   Note 2    13.56 MHz

  7D01        HID iCLASS CSN                                      Note 2    13.56 MHz

  7E01        I-Code CSN (Philips/NXP)                            Note 2    13.56 MHz

  7E11        my-d CSN (Infineon)                                 Note 2    13.56 MHz

  7E21        ISO 15693 CSN                                       Note 2    13.56 MHz

  7E31        etag CSN (Secura Key)                               Note 2    13.56 MHz

  7E41        Tag-It CSN (Texas Instruments)                      Note 2    13.56 MHz

  7F02        Topaz CSN (NFC 1)                                   Note 23   13.56 MHz

  7F01        DESFire CSN (Oyster/NFC 4)                          Note 2    13.56 MHz

  7F11        I-tag CSN (IBM)                                     Note 2    13.56 MHz

  7F21        ISO 14443A CSN                                      Note 2    13.56 MHz

  7F31        Advant CSN (Legic)                                  Note 2    13.56 MHz

  7F41        MiFare CSN (Philips/NXP)                            Note 2    13.56 MHz

  7F51        MiFare Ultralight CSN (Philips/NXP/NFC 2)           Note 2    13.56 MHz

  7C02        FeliCa(NFC 3)                                       Note 23   13.56 MHz

  7B01        ISO 14443B CSN                                      Note 23   13.56 MHz

  7A01        CEPAS                                               Note 23   13.56 MHz

  E401        ID Teck                                             Note 25   125 kHz

  E902        Paradox                                             Note 3    125 kHz

  E922        Postech                                             Note 3    125 kHz

  E912        CDVI                                                Note 3    125 kHz

  EA01        Keri NXT UID                                        Note 3    125 kHz

  EA02        Keri PSC-1 26 Bit                                   Note 4    125 kHz

  EA11        Pyramid (Farpointe Data) UID                        Note 4    125 kHz

  EA12        Pyramid (Farpointe Data) PSC-1 26 Bit               Note 4    125 kHz

  EA21        Farpointe Data (Pyramid) UID                        Note 4    125 kHz

  EA22        Farpointe Data (Pyramid) PSC-1 26 Bit :             Note 4    125 kHz
                                                                            
              RDR-647x Compatible                                           

  EB02        Radio Key (Secura Key -02) RKCx-02                  Note 5    125 kHz

  EC01        Secura Key -01 RKCx-01                              Note 5    125 kHz

  ED02        Indala ASP+ UID (Motorola)                          Note 22   125 kHz

  ED05        Indala ASP+ Custom                                  Note 22   125 kHz

  EF04        HID Prox                                            Note 7    125 kHz

  F004        ReadyKey Pro UID                                    Note 8    125 kHz

  F101        Dimpna UID                                          Note 23   125 kHz

  F201        HiTag 2                                             Note 9    125 kHz

  F204        HiTag 2 Alternate                                   Note 26   125 kHz

  F302        HiTag 1 & S                                         Note 9    125 kHz

  F304        HiTag 1 & S Alternate                               Note 9    125 kHz

  F502        GProx-II UID                                        Note 10   125 kHz

  F504        GProx-II ID                                         Note 23   125 kHz

  F602        Cardax UID                                          Note 11   125 kHz

  F612        Russwin UID                                         Note 11   125 kHz

  F712        Nexwatch (Honeywell)                                Note 12   125 kHz

  F722        NexKey/Quadrakey/ KeyMate/2Smart Key (Honeywell)    Note 12   125 kHz

  F802        Keri UID : RDR-6K8x Compatible                      Note 13   125 kHz

  F902        ioProx (Kantech)                                    Note 14   125 kHz

  FA02        Awid                                                Note 15   125 kHz

  FB01        EM 410x                                             Note 16   125 kHz

  FB02        EM 410x Alternate                                   Note 17   125 kHz

  FB11        DIGITAG                                             Note 18   125 kHz

  FB21        Rosslare                                            Note 18   125 kHz

  FC02        CASI-RUSCO (GE Security/UTC)                        Note 19   125 kHz

  FC03        URMET                                               Note 23   125 kHz

  FD01        Indala ASP UID (Motorola)                           Note 22   125 kHz

  FD02        Indala ASP 26 bit (Motorola)                        Note 21   125 kHz

  FD04        Indala ASP Custom                                   Note 22   125 kHz

  FD05        Indala ECR Custom                                   Note 22   125 kHz

  DF01        Cotag                                               Note 23   133 kHz

  E301        Nedap                                               Note 23   125 kHz

  E402        ID Teck Alternate                                   Note 23   125 kHz

  F401        Deister UID                                         Note 23   125 kHz

  FA03        ISONAS                                              Note 23   125 kHz
  -------------------------------------------------------------------------------------

Table 2

Reader list Notes:
------------------

> The notes reference in table 2 provides a list of readers for each
> Card Type.

iCLASS card rules:
------------------

> <span id="OLE_LINK1" class="anchor"><span id="OLE_LINK2"
> class="anchor"></span></span>The iClass Card Types and supporting
> readers shall follow these rules.

-   ###  Description for the supporting readers when a HID iClass Card Type is detected.

> RDR-708x for iCLASS ID
>
> RDR-758x for iCLASS CSN
>
> RDR-805xx for iCLASS CSN

-   ###  Description for the supporting readers when a HID iClass SEOS Card Type is detected.

> RDR-800xx for iCLASS ID (SEOS requires FW LNCxxxxxx)

###  Description for Card Types ISO14443A or RDR-758x Equivalent are detected.

> If the Card Analyzer detects ISO14443A card. Save the raw data; reboot
> and detect the card again without changing the Card Type. After
> detecting the card a second time; compare the raw data to the data
> collected the first time. If they match; add the cards to the list of
> detected Card Types following the rules in section 11.4 of this
> document.
>
> Design:
>
> • After detecting card, check if it is the ISO14443A Card Type.
>
> • If yes, then save the RAW data.
>
> • Reboot the reader.
>
> • Write the same ISO card again.
>
> • Get the RAW data and compare with older RAW data.
>
> • If both are the same; list the ISO cards following the rules in
> section 11.4 of this document; otherwise they aren’t listed in the
> detected card Type list.

Filter rules for ISO 14443A CSN and RDR-758x Equivalent cards on the learn Card Type screen
-------------------------------------------------------------------------------------------

> If the detected Card Type is ISO 14443A CSN and the number of bits
> equal 56; the following list of Card Types shall be displayed.

  Card size is 56 bits                                           
  ------------------------------------------------------- ------ ------
  ISO 14443A CSN                                          7Fx1   7F21
                                                                  
    DESFire CSN                                                  7F01
    I-tag CSN (IBM)                                              7F11
    MiFare CSN (Philips, NXP)                                    7F41
    MiFare Ultralight CSN (Philips, NXP)                         7F51
  RDR-758x Equivalent (iClass, ISO14443A, ISO15693) CSN   6Fx1   6F01

> If the detected Card Type is ISO 14443A CSN and the number of bits
> equals 32; the following list of Card Types shall be displayed.

  Card size is 32 bits                                           
  ------------------------------------------------------- ------ ------
  ISO 14443A CSN                                          7Fx1   7F21
    Advant CSN (Legic)                                           7F31
                                                                  
                                                                  
    MiFare CSN (Philips, NXP)                                    7F41
                                                                  
  RDR-758x Equivalent (iClass, ISO14443A, ISO15693) CSN   6Fx1   6F01

 Indala, ID Teck rules:
-----------------------

> Because of a firmware issue that causes the reader to detect ID Teck
> Card Type as Indala Card Types. The 125 kHZ Card Types shall scan
> starting with the FD05 to E401. If the application has already
> detected an Indala Card Type, it shall not scan for the ID Teck card.

Rules for detecting ISONAS and AWID Card Types
----------------------------------------------

### Change the search getAccess list so we always look for the AWID card first; no matter the reader we are connected too.  {#change-the-search-getaccess-list-so-we-always-look-for-the-awid-card-first-no-matter-the-reader-we-are-connected-too. .ListParagraph}

> If the application has already detected an ISONAS, it shall not scan
> for the AWID card.

Notes for displaying the supporting readers:
--------------------------------------------

> **Note 1:**
>
> RDR-708x for iCLASS ID/CSN
>
> RDR-758x for iCLASS CSN
>
> RDR-805xx for iCLASS CSN
>
> **Note 2:** Readers RDR-75xx, RDR-805xx, RDR-800xx, RDR-708x
>
> **Note 3:** Readers RDR-805xx, RDR-800xx
>
> **Note 4:** Readers RDR-647x, RDR-805xx, RDR-800xx
>
> **Note 5:** Readers RDR-6Z8x, RDR-805xx, RDR-800xx
>
> **Note 6:** Readers RDR-638x, RDR-805xx, RDR-800xx
>
> **Note 7:** Readers RDR-608x, RDR-805xx, RDR-800xx
>
> RDR-601x, RDR-602x
>
> **Note 8:** Readers RDR-6R8x, RDR-805xx, RDR-800xx
>
> **Note 9:** Readers RDR-6H8x, RDR-805xx, RDR-800xx
>
> **Note 10:** Readers RDR-6G8x, RDR-805xx, RDR-800xx
>
> **Note 11:** Readers RDR-6C8x, RDR-805xx, RDR-800xx
>
> **Note 12:** Readers RDR-6N8x, RDR-805xx, RDR-800xx
>
> **Note 13:** Readers RDR-6K8x, RDR-805xx, RDR-800xx
>
> **Note 14:** Readers RDR-678x, RDR-805xx, RDR-800xx
>
> **Note 15:** Readers RDR-698x, RDR-805xx, RDR-800xx,
>
> RDR-691x, RDR-692x
>
> **Note 16:** Readers RDR-6E8x, RDR-805xx, RDR-800xx
>
> RDR-6E1x, RDR-6E2x
>
> **Note 17:** Readers RDR-805xx, RDR-800xx
>
> **Note 18:** Readers RDR-6E8x, RDR-805xx, RDR-800xx
>
> **Note 19:** Readers RDR-628x, RDR-805xx, RDR-800xx
>
> **Note 20:** Readers RDR-638x, RDR-805xx, RDR-800xx
>
> **Note 21:** Readers RDR-638x, RDR-805xx, RDR-800xx,
>
> RDR-631x, RDR-632x
>
> **Note 22**: Please contact RF IDeas sales
>
> (Card Types using this note shall not be displayed in the Auto Config
> or Analyze Card screens card list)
>
> **Note 23:** For the new Card Types supported in the new Loon readers:
>
> RDR-805xx (requires FW LNCxxxxxx)
>
> RDR-800xx for iCLASS ID (SEOS requires FW LNCxxxxxx)
>
> **Note 24:** For SEOS Card Type:
>
> RDR-800xx for iCLASS ID (SEOS requires FW LNCxxxxxx)
>
> **Note 25:** Readers RDR-6A8x,RDR-805xx,RDR-800xx
>
> **Note 26:** Readers RDR-805xx, RDR-800xx

Analyze Card:
=============

> The application shall attempt to locate the Card ID and FAC, using the
> raw data captured during the “Learn Card” process. The user can
> highlight a Card Type in the Card Type field they wish to analyze.
>
> The user inputs, Card ID and FAC are used to locate the ID and create
> the settings for LP, TP, Reverse or Invert Bit, Reverse Bytes, the “ID
> field bit count” with the “ID extended precision math on” enable.
>
> The application shall use the user input “Card ID” as a starting value
> for the number of bits in the “Card ID”. After locating the “Card ID”
> in the raw data, the application shall adjust the “ID field bit count”
> by counting and adding the number of zero’s on the left of the “Card
> ID” to the “ID field bit count”.

Example 1:
----------

> **Learn Card**
>
> Bits 26: 00.00.00.00.00.6C.7C.DB: HID Prox, HID Prox UID, RDR-608x,
> RDR-8058x, RDR-8008x
>
> **Analyze Tab**
>
> **Analyze        ID 15981                 FAC 54**
>
> Bits 26:  00.00.00.00.00.6C.7C.DB: HID Prox: Formatted 54:15981  
>
> How I determine card format:
>
> 00011011000111110011011011 
>
> 00011011000111110011011010
>
> (FAC54)          (ID15981)
>
> Result: LP =1, TP=1 and the Invert Bits = checked or enabled.

Example 2 Padding:
------------------

A.  The raw data, ID and the FAC values use to locate the ID or ID
    and FAC.

B.  The FAC number is located by rotating the FAC value entered by the
    user from MSB to LSB until a bit match is obtained.

C.  In this example the ID is less than 8 bits, so the application must
    add 6 leading zero. If using the “Extended” routines, increase the
    window size from 2 to 8 bits, add padding.

D.  The “ID field bit count” is determined by adding the number of zeros
    found after the ID to the ID plus padding value.

 Example 3 Using FAC and ID:\
-----------------------------

A.  The raw data, ID and the FAC values use to locate the ID or ID
    and FAC.

B.  The FAC number is located by rotating the FAC value entered by the
    user from MSB to LSB until a bit match is obtained.

C.  In this example the ID contains more than 8 bits, no padding
    is required. If using the “Extended” routines, set the “Number of
    bits” equal to the number of bits in the ID or ID/FAC entered by
    the user.

D.  The “ID field bit count” is determined by adding the number of zeros
    found after the ID to the number of bits in the ID.

![](media/image18.png){width="6.051327646544182in"
height="5.020206692913386in"}

> Figure 9

Text over picture Analyze Screen:
---------------------------------

> The following text shall be displayed over the reader picture (Figure
> 9).
>
> “In this step we will attempt to learn the reader settings for the
> selected card.
>
> 1: Enter the Card ID and FAC numbers in the “User Input Field”, the
> FAC may be omitted if unknown.
>
> 2: Press the “Analyze” button to begin the search. The results are
> displayed in the “Analyzed Card ID” field.
>
> 3: Select the Configuration \# and press the “Write” button to write
> the settings to the reader.
>
> 4: Press the “Auto Config” button to transition to Auto Config screen
>
> 5: Press the “Exit” button to stop the Card Analyzer and return to the
> configuration Utility.”

![](media/image19.png){width="6.113819991251094in"
height="5.041036745406824in"}

Figure 10

Analyze Button on the Analyze Card Screen:
------------------------------------------

> The “Analyze” button is greyed out and unavailable. The “Analyze”
> button shall become active after the user, enters the FAC, Card ID or
> both (Figure 10) . Pressing this button will start the analyze
> process.

<span id="_Toc434490608" class="anchor"><span id="_Toc459197090" class="anchor"></span></span>Auto Config Button on the Analyze Card Screen:
--------------------------------------------------------------------------------------------------------------------------------------------

> User can switch from Analyzer screen to Auto Config screen. (Figure
> 11).

Card Type Field:
----------------

> The field contains a list of Card Types detected during the “Learn
> Card” process. The field is a list box (Figures 9) that allows the
> user to select one of the available Card Types to analyze.

Analyzed Card ID:
-----------------

> The “Analyzed Card ID” field shall display the formatted data found
> using the search table (Table 3) for the Card Type selected in the
> “Card Type” field (Figure 11).
>
> Example:
>
> Card ID Found: 999999
>
> **If Card ID wasn’t located**:
>
> Unable to locate the Card ID: Please contact RF IDeas for support.
>
> ![](media/image20.png){width="5.978419728783902in"
> height="4.978544400699913in"}
>
> Figure 11

Write Reader button:
--------------------

> This button displays information on the Card Types selected from the
> Card Type field; it’s not a drop down list. The “Write” button is used
> in conjunction with the “Configuration \#” selection field. Pressing
> the “Write” button will write the configuration to the reader.
> Changing the configuration number will display the Card Type written
> to the reader.

<span id="_Toc434490612" class="anchor"><span id="_Toc459197094" class="anchor"></span></span>Analyze Screen - Save Hwg + Button:
---------------------------------------------------------------------------------------------------------------------------------

> The Save HWG file button will always save the temporary file to a HWG
> file with current reader settings when pressed, to the specified
> location.

Exit Button Analyze Card Type Screen: 
--------------------------------------

> 1\. If no configurations where written to the reader, the application
> shall reload the saved HWG+ file.
>
> 2\. If one configuration was written to the reader, the other shall be
> remained to original settings. The application should not reload the
> saved HWG+ file.
>
> 3\. If any of the configurations were written to the reader. The
> application shall not reload the HWG+ file to the reader..

Analyze Algorithm Flow:
-----------------------

> 1\) The application shall search for the Card ID, FAC or both using raw
> data, LP = 0 and TP = 0.
>
> 2\) No parity will be used during this search. LP and TP values will be
> determine after the search ends.
>
> 3\) If the user ID bit length is less than 8, pad the number with leading
> zeros to make it 8 bits otherwise leave the user input (ID bits)
> unchanged.
>
> **Located the FAC and ID; no padding needed for the FAC value. **
>
> 4\) The search combinations shall follow these rules:
>
> \(a) Inverted Bits ON
>
> \(b) Inverted Bits OFF
>
> \(c) Inverted Bits ON + Reverse Bits ON
>
> \(d) Inverted Bits OFF + Reverse Bits ON
>
> **(Execute step (e) when the raw data is a multiple of 8)**
>
> \(e) Inverted Bits ON + Reverse Byte ON.
>
> 5\) Search for the "User ID Bits" starting with the MSB to LSB in the Raw
> Data. If an ID match occurs, search for the "User FAC Bits" starting
> from the MSB to the LSB in the remaining Raw Data.
>
> If we find the FAC, add the remaining zeroes to the "ID field bit
> length" and set the LP, TP, ID Field bit count, Invert and reverse
> bit/byte to the current settings. Otherwise, if the FAC doesn't match,
> go to the next combination of bit settings in step 4.
>
> **For Card ID Search Only:**
>
> After finding the ID, the application shall determine the additional
> bits available for the ID bit range, by counting the number of zero
> bits to the left of the ID. The number of zero found after the ID and
> the number of bits enter for the ID are added together to create the
> ID field bit count.
